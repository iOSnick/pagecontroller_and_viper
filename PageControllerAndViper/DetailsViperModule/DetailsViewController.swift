//
//  DetailsViewController.swift
//  PageControllerAndViper
//
//  Created by Vikhlyaev Serge on 22.10.2021.
//

import UIKit

/// Протокол управления view-слоем
protocol DetailsDisplayingProtocol: AnyObject {

	/// Метод, изменяющий счетчик под актуальное количество заказов
	/// - Parameter count: Новое значение кол-ва заказов
	func changeDisplayingOrdersCount(_ count: Int)

	/// Метод, изменяющий индекс выбранного заказа в pageControl
	/// - Parameter indexOfCurrentOrder: Новое значение индекса
	func changeIndexOfDisplayingOrder(_ indexOfCurrentOrder: Int)

	/// Метод, обновляющий массив ВСЕХ viewModel-ей, отображаемых в карусели
	/// - Parameter pages: viewModel-и заказов
	func pushAllOrdersAsPages(_ pages: [PageViewModelProtocol])

	/// Метод для фокусирования на конкретном заказе
	/// - Parameter index: индекс заказа
	func displaySelectedOrder(_ index: Int)

	/// Метод отображения ошибки во view-слое
	/// - Parameters:
	///   - title: Заголовок (обязательно)
	///   - subtitle: Подзаголовок
	func displayError(title: String, subtitle: String?)
}

/// Класс ViewController-а VIPER-модуля  детальной информации о заказе
final class DetailsViewController: UIViewController {

	enum HeaderColor {
		case blue
		case green
		case white
	}

	@IBOutlet weak var headerView: UIView!
	@IBOutlet weak var idLabel: UILabel!
	@IBOutlet weak var orderNameLabel: UILabel!
	@IBOutlet weak var orderImageView: UIImageView!
	@IBOutlet weak var pageControl: UIPageControl!
	@IBOutlet weak var collectionView: UICollectionView!

	private var gradientLayer: CAGradientLayer? {
		didSet {
			guard let gradientLayer = gradientLayer else { return }
			guard let oldValue = oldValue else {
				headerView.layer.insertSublayer(gradientLayer, at:0)
				return
			}
			headerView.layer.replaceSublayer(oldValue, with: gradientLayer)
		}
	}

	private var gradientFromValue: [CGColor] = []

	/// Массив viewModel-ей
	private var pages: [PageViewModelProtocol] = [] {
		didSet {
			collectionView.reloadData()
		}
	}

	var presenter: DetailsEventIntersepter?
	var isApeared = false
	var callbackWhenAppeared: (() -> Void)? {
		didSet {
			isApeared ? callbackWhenAppeared?() : ()
		}
	}

	// MARK: - Init
	override init(nibName nibNameOrNil: String?, bundle nibBundleOrNil: Bundle?) {
		super.init(nibName: nibNameOrNil, bundle: nibBundleOrNil)
	}

	required init?(coder: NSCoder) {
		super.init(coder: coder)
	}

	// MARK: - Lifecycle
	override func viewDidLoad() {
		super.viewDidLoad()
		firstSetupUI()
		setupCollectionView()
	}

	override func viewWillAppear(_ animated: Bool) {
		super.viewWillAppear(animated)
		let appearance = UINavigationBarAppearance()
		appearance.configureWithOpaqueBackground()
		appearance.backgroundColor = .clear
		navigationController?.navigationBar.standardAppearance = appearance
		navigationController?.navigationBar.scrollEdgeAppearance = navigationController?.navigationBar.standardAppearance
		navigationController?.navigationBar.barTintColor = UIColor.white
		title = "Ваши заказы"
		let backButton = UIBarButtonItem()
		backButton.tintColor = .white
		backButton.title = ""
		self.navigationController?.navigationBar.topItem?.backBarButtonItem = backButton
		presenter?.notifyViewDidLoad()
	}

	override func viewDidAppear(_ animated: Bool) {
		super.viewDidAppear(animated)
		isApeared = true
		callbackWhenAppeared?()
	}
}

// MARK: - Private
private extension DetailsViewController {

	/// Первоначальная настройка UI
	func firstSetupUI() {
		setHeaderColor(.white, for: headerView)
		idLabel.text = ""
		orderNameLabel.text = ""
		pageControl.numberOfPages = 0
	}

	func setupCollectionView() {
		collectionView.delegate = self
		collectionView.dataSource = self
		registerCells()
	}

	func registerCells() {
		let xibs = [
			DetailsPageCollectionViewCell.self
		]

		xibs.forEach({
			collectionView.register(UINib(nibName: String(describing: $0),
										  bundle: nil),
									forCellWithReuseIdentifier: String(describing: $0))
		})
	}

	/// Метод демонстрации информации об ошибке
	/// - Parameters:
	///   - title: Заголовок alert-а
	///   - subtitle: Подзаголовок alert-а
	func showAlert(title: String, subtitle: String?) {
		let alertController = UIAlertController(title: title, message: subtitle, preferredStyle: .alert)
		present(alertController, animated: true, completion: nil)
	}

	func configureHeader(for page: PageViewModelProtocol) {
		idLabel.text = page.idLabelText
		orderNameLabel.text = page.orderNameLabelText
		UIView.animate(withDuration: 2) { [weak self] in
			guard let self = self else { return }
			self.setHeaderColor(page.headerColor, for: self.headerView)
		}
	}

	func setHeaderColor(_ color: HeaderColor, for headerView: UIView) {
		switch color {
		case .blue:
			let gradientChangeAnimation = CABasicAnimation(keyPath: "colors")
			gradientChangeAnimation.duration = 0.5
			let colorTop = UsingColors.blue
			let colorBottom = UIColor.white.cgColor
			gradientChangeAnimation.fromValue = gradientFromValue
			let toValue = [
				colorTop,
				colorBottom
			]
			gradientChangeAnimation.toValue = toValue
			gradientFromValue = toValue
			gradientChangeAnimation.fillMode = CAMediaTimingFillMode.forwards
			gradientChangeAnimation.isRemovedOnCompletion = false
			gradientLayer?.add(gradientChangeAnimation, forKey: "colorChange")
		case .green:
			let gradientChangeAnimation = CABasicAnimation(keyPath: "colors")
			gradientChangeAnimation.duration = 0.5
			let colorTop = UsingColors.green
			let colorBottom = UIColor.white.cgColor
			gradientChangeAnimation.fromValue = gradientFromValue
			let toValue = [
				colorTop,
				colorBottom
			]
			gradientChangeAnimation.toValue = toValue
			gradientFromValue = toValue
			gradientChangeAnimation.fillMode = CAMediaTimingFillMode.forwards
			gradientChangeAnimation.isRemovedOnCompletion = false
			gradientLayer?.add(gradientChangeAnimation, forKey: "colorChange")
		case .white:
			let colorTop = UIColor.white.cgColor
			let colorBottom = UIColor.white.cgColor
			let gradient = CAGradientLayer()
			gradient.colors = [colorTop, colorBottom]
			gradient.locations = [0.0, 1.0]
			gradient.frame = headerView.bounds
			gradientFromValue = [colorTop, colorBottom]
			gradientLayer = gradient
		}
	}
}

// MARK: - DetailsDisplayingProtocol
extension DetailsViewController: DetailsDisplayingProtocol {
	func changeDisplayingOrdersCount(_ count: Int) {
		pageControl.numberOfPages = count
	}

	func changeIndexOfDisplayingOrder(_ indexOfCurrentOrder: Int) {
		pageControl.currentPage = indexOfCurrentOrder
	}

	func pushAllOrdersAsPages(_ pages: [PageViewModelProtocol]) {
		self.pages = pages
	}

	func displaySelectedOrder(_ index: Int) {
		callbackWhenAppeared = { [weak self] in
			guard index < self?.pages.count ?? -1 else { return }
			let indexPath = IndexPath(row: index, section: 0)
			self?.pageControl.currentPage = indexPath.row
			self?.collectionView.scrollToItem(at: indexPath, at: .centeredHorizontally, animated: true)
			guard let page = self?.pages[indexPath.row] else { return }
			self?.configureHeader(for: page)
		}
	}

	func displayError(title: String, subtitle: String?) {
		showAlert(title: title, subtitle: subtitle)
	}
}

// MARK: - UICollectionViewDelegate & UICollectionViewDataSource & UICollectionViewDelegateFlowLayout
extension DetailsViewController: UICollectionViewDelegate &
								 UICollectionViewDataSource &
								 UICollectionViewDelegateFlowLayout {

	func collectionView(_ collectionView: UICollectionView,
						numberOfItemsInSection section: Int) -> Int {
		pages.count
	}

	func collectionView(_ collectionView: UICollectionView,
						cellForItemAt indexPath: IndexPath) -> UICollectionViewCell {

		let cell = collectionView.dequeueReusableCell(withReuseIdentifier: String(describing: DetailsPageCollectionViewCell.self), for: indexPath) as! DetailsPageCollectionViewCell
		cell.configure(with: pages[indexPath.row].operations,
					   operationsColor: pages[indexPath.row].headerColor == .blue ? UsingColors.blue : UsingColors.green,
					   eventIntercepter: pages[indexPath.row].selectOperationEventIntercepter)

		return cell
	}

	func collectionView(_ collectionView: UICollectionView,
						layout collectionViewLayout: UICollectionViewLayout,
						sizeForItemAt indexPath: IndexPath) -> CGSize {

		CGSize(width: collectionView.frame.width, height: collectionView.frame.height)
	}

	func collectionView(_ collectionView: UICollectionView,
						layout collectionViewLayout: UICollectionViewLayout,
						minimumLineSpacingForSectionAt section: Int) -> CGFloat {
		0
	}

	func collectionView(_ collectionView: UICollectionView,
						layout collectionViewLayout: UICollectionViewLayout,
						insetForSectionAt section: Int) -> UIEdgeInsets {

		UIEdgeInsets(top: 0, left: 0, bottom: 0, right: 0)
	}

	func scrollViewDidEndDecelerating(_ scrollView: UIScrollView) {
		var currentCellOffset = collectionView.contentOffset
		currentCellOffset.x += collectionView.frame.size.width / 2
		guard let indexPath = collectionView.indexPathForItem(at: currentCellOffset) else { return }
		pageControl.currentPage = indexPath.row
		configureHeader(for: pages[indexPath.row])
		collectionView.scrollToItem(at: indexPath, at: .centeredHorizontally, animated: true)
	}
}
