//
//  OperationCollectionViewCell.swift
//  PageControllerAndViper
//
//  Created by 18907889 on 29.10.2021.
//

import UIKit

/// Класс ячейки операции
final class OperationCollectionViewCell: UICollectionViewCell {

	@IBOutlet weak var operationImageView: UIImageView!

	private (set) var operation: Order.AvailableOperation? {
		didSet {
			configureCellUI()
		}
	}

	override func awakeFromNib() {
		super.awakeFromNib()
		operationImageView.image = nil
		self.layer.cornerRadius = 8
		self.layer.masksToBounds = true
	}

	/// Конфигурация ячейки операции
	/// - Parameters:
	///   - operation: тип операции
	func configure(with operation: Order.AvailableOperation) {
		self.operation = operation
	}

	override func prepareForReuse() {
		super.prepareForReuse()
		operationImageView.image = nil
	}
}

// MARK: - Private
private extension OperationCollectionViewCell {
	func configureCellUI() {
		switch operation {
		case .cancel:
			operationImageView.image = UIImage(systemName: "clear")
		case .repeatOrder:
			operationImageView.image = UIImage(systemName: "repeat.circle")
		case .sendFeedback:
			operationImageView.image = UIImage(systemName: "pencil.circle")
		case .call:
			operationImageView.image = UIImage(systemName: "phone.bubble.left")
		case .none:
			operationImageView.image = nil
		}
	}
}
