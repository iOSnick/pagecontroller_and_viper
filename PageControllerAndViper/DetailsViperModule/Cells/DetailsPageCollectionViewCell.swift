//
//  DetailsPageCollectionViewCell.swift
//  PageControllerAndViper
//
//  Created by 18907889 on 01.11.2021.
//

import UIKit

/// Ячейка страницы детальной информации по заказу
final class DetailsPageCollectionViewCell: UICollectionViewCell {

	@IBOutlet weak var tableView: UITableView!

	private var eventIntercepter: SelectOperationEventInterceptable?
	private var operationsColor: CGColor?
	private var operations: [Order.AvailableOperation] = [] {
		didSet {
			tableView.reloadData()
		}
	}

	override func awakeFromNib() {
		super.awakeFromNib()
		backgroundColor = .clear
		setupTableView()
	}

	func configure(with operations: [Order.AvailableOperation],
				   operationsColor: CGColor?,
				   eventIntercepter: SelectOperationEventInterceptable?) {
		self.eventIntercepter = eventIntercepter
		self.operationsColor = operationsColor != nil ? operationsColor : UIColor.white.cgColor
		self.operations = operations
	}
}

private extension DetailsPageCollectionViewCell {
	func setupTableView() {
		tableView.delegate = self
		tableView.dataSource = self
		registerCells()
	}

	func registerCells() {
		let xibs = [
			OperationsTableViewCell.self
		]

		xibs.forEach({
			tableView.register(UINib(nibName: String(describing: $0),
									 bundle: nil),
							   forCellReuseIdentifier: String(describing: $0))
		})
	}
}

// MARK: - UITableViewDelegate & UITableViewDataSource
extension DetailsPageCollectionViewCell: UITableViewDelegate & UITableViewDataSource {
	func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
		var count = 0
		if !operations.isEmpty {
			count += 1
		}

		return count
	}

	func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
		if indexPath.row == 0 {
			let cell = tableView.dequeueReusableCell(withIdentifier: String(describing: OperationsTableViewCell.self), for: indexPath) as! OperationsTableViewCell
			cell.configure(with: operations,
						   color: operationsColor,
						   eventIntercepter: eventIntercepter)
			return cell
		}

		return UITableViewCell()
	}

	func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat {
		indexPath.row == 0 ? 120 : UITableView.automaticDimension
	}
}
