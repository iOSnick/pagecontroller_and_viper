//
//  OperationsTableViewCell.swift
//  PageControllerAndViper
//
//  Created by 18907889 on 29.10.2021.
//

import UIKit

/// Ячейка с доступными операциями по заказу
final class OperationsTableViewCell: UITableViewCell {

	@IBOutlet weak var collectionView: UICollectionView?

	private var eventIntercepter: SelectOperationEventInterceptable?
	private var color: CGColor?
	private var operations: [Order.AvailableOperation] = [] {
		didSet {
			collectionView?.reloadData()
		}
	}

	override func awakeFromNib() {
		super.awakeFromNib()
		setupCollectionView()
		registerCells()
	}

	override func setSelected(_ selected: Bool, animated: Bool) {
		super.setSelected(selected, animated: animated)
	}

	func configure(with operations: [Order.AvailableOperation],
				   color: CGColor?,
				   eventIntercepter: SelectOperationEventInterceptable?) {
		self.color = color
		self.eventIntercepter = eventIntercepter
		self.operations = operations
	}
}

// MARK: - Private
private extension OperationsTableViewCell {

	func setupCollectionView() {
		collectionView?.backgroundView?.backgroundColor = .clear
		collectionView?.delegate = self
		collectionView?.dataSource = self
	}

	func registerCells() {
		let usingCellNames = [
			String(describing: OperationCollectionViewCell.self)
		]

		usingCellNames.forEach({
			collectionView?.register(UINib(nibName: $0, bundle: nil), forCellWithReuseIdentifier: $0)
		})
	}
}

// MARK: - UICollectionViewDelegate & UICollectionViewDataSource
extension OperationsTableViewCell: UICollectionViewDelegate &
								   UICollectionViewDataSource &
								   UICollectionViewDelegateFlowLayout {

	func collectionView(_ collectionView: UICollectionView, numberOfItemsInSection section: Int) -> Int {
		operations.count
	}

	func collectionView(_ collectionView: UICollectionView, cellForItemAt indexPath: IndexPath) -> UICollectionViewCell {
		let cell = collectionView.dequeueReusableCell(withReuseIdentifier: String(describing: OperationCollectionViewCell.self), for: indexPath) as! OperationCollectionViewCell
		cell.configure(with: operations[indexPath.row])

		if let color = color {
			cell.backgroundColor = UIColor(cgColor: color)
		}
		return cell
	}

	func collectionView(_ collectionView: UICollectionView, didSelectItemAt indexPath: IndexPath) {
		collectionView.deselectItem(at: indexPath, animated: true)
		guard
			let cell = collectionView.cellForItem(at: indexPath) as? OperationCollectionViewCell,
			let operation = cell.operation
		else { return }

		eventIntercepter?.didSelectOperation(operation)
	}

	func collectionView(_ collectionView: UICollectionView, layout collectionViewLayout: UICollectionViewLayout, sizeForItemAt indexPath: IndexPath) -> CGSize {
			return CGSize(width: 100, height: 100)
		}
}
