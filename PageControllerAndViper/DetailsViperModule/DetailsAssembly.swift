//
//  DetailsAssembly.swift
//  PageControllerAndViper
//
//  Created by Vikhlyaev Serge on 22.10.2021.
//

import UIKit

/// Протокол сборки (VIPER) модуля
protocol AssemblyProtocol {

	/// Метод сборки модуля на базе объекта UIViewController
	/// - Parameter view: UIViewController, для которого мы собираем модуль
	func configure(view: inout UIViewController)
}

final class DetailsAssembly {

	private let orderID: String

	/// Инициализатор сборщика VIPER-модуля экрана детальной информации
	/// - Parameter orderID: id заказа, который был выбран для
	/// отображения на pageController-е при запуске
	init(orderID: String) {
		self.orderID = orderID
	}
}

/// MARK: - AssemblyProtocol
extension DetailsAssembly: AssemblyProtocol {
	func configure(view: inout UIViewController) {
		guard let view = view as? DetailsViewController else { return }
		let presenter = DetailsPresenter()
		let interactor = DetailsInteractor(selectedOrderID: orderID)
		let router = DetailsRouter()

		view.presenter = presenter
		presenter.interactor = interactor
		presenter.view = view
		presenter.router = router
		interactor.presenter = presenter
	}
}


