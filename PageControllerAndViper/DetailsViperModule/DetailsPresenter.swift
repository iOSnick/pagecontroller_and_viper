//
//  DetailsPresenter.swift
//  PageControllerAndViper
//
//  Created by Vikhlyaev Serge on 22.10.2021.
//

/// Протокол отслеживания событий view-слоя
protocol DetailsEventIntersepter {

	/// Нотифицирующий метод о готовности view-слоя
	func notifyViewDidLoad()
}

/// Протокол взаимодействия со слоем презентации
protocol DetailsPresentationProtocol: AnyObject {

	/// Отправка во view количества отображаемых заказов
	/// - Parameter count: кол-во заказов
	func pushCountOfOrders(_ count: Int)

	/// Отправка во view индекса выбранного заказа
	/// - Parameter indexOfCurrentOrder: индекс выбранного заказа
	func pushIndexOfCurrentOrder(_ indexOfCurrentOrder: Int)

	/// Метод отправки во view всех заказов к отображению
	/// - Parameter orders: заказы
	func pushAllOrders(_ orders: [Order])

	/// Метод, инициирующий отображение выбранного заказа
	func presentCurrentSelectedOrder(at index: Int)

	/// Функция отображения ошибки по результату запроса данных
	/// - Parameter errorMessage: Текстовое сообщение
	/// - Parameter errorCase: Внутренняя нотация об ошибке
	func present(_ errorMessage: String?, _ errorCase: DetailsInteractor.ErrorCase)
}

/// Протокол выбора операций по заказу
protocol SelectOperationEventInterceptable {

	/// Была выбрана операция
	func didSelectOperation(_ operation: Order.AvailableOperation)
}

/// Класс presenter-а VIPER модуля детальной информации о заказе
final class DetailsPresenter {

	/// Ссылка на слой отображения
	weak var view: DetailsDisplayingProtocol?

	/// Ссылка на бизнес-слой
	var interactor: DetailsInteractionProtocol?

	/// Ссылка на слой роутинга
	var router: DetailsRoutingProtocol?
}

// MARK: - DetailsEventIntersepter
extension DetailsPresenter: DetailsEventIntersepter {
	func notifyViewDidLoad() {
		interactor?.notifyPresenterReady()
	}
}

// MARK: - DetailsPresentationProtocol
extension DetailsPresenter: DetailsPresentationProtocol {
	func pushCountOfOrders(_ count: Int) {
		view?.changeDisplayingOrdersCount(count)
	}

	func pushIndexOfCurrentOrder(_ indexOfCurrentOrder: Int) {
		view?.changeIndexOfDisplayingOrder(indexOfCurrentOrder)
	}

	func pushAllOrders(_ orders: [Order]) {
		let pageViewModels = orders.compactMap({
			PageViewModel(id: $0.orderID,
						  orderName: $0.orderTitle,
						  imageURL: nil,
						  orderType: $0.orderType,
						  operations: $0.operations,
						  interceptor: self)
		})
		view?.pushAllOrdersAsPages(pageViewModels)
	}

	func presentCurrentSelectedOrder(at index: Int) {
		view?.displaySelectedOrder(index)
	}

	func present(_ errorMessage: String?, _ errorCase: DetailsInteractor.ErrorCase) {
		guard let view = view else { return }
		guard let errorMessage = errorMessage else {
			view.displayError(title: errorCase.rawValue, subtitle: "Текст ошибки отсутствует")
			return
		}

		view.displayError(title: errorCase.rawValue, subtitle: errorMessage)
	}
}

// MARK: - SelectOperationEventInterceptable
extension DetailsPresenter: SelectOperationEventInterceptable {
	func didSelectOperation(_ operation: Order.AvailableOperation) {
		
	}
}
