//
//  DetailsInteractor.swift
//  PageControllerAndViper
//
//  Created by Vikhlyaev Serge on 22.10.2021.
//

/// Протокол взаимодействием со слоем бизнес-логики из presenter-а
protocol DetailsInteractionProtocol {
	func notifyPresenterReady()
}

/// Класс бизнес-логики экрана детальной информации о заказе
final class DetailsInteractor {

	/// Перечень возможных ошибок
	enum ErrorCase: String {
		/// Неизвестная ошибка
		case unknownError = "Неизвестная ошибка"
		/// Ошибка на стороне сервера
		case backendError = "Возможно ошибка на стороне сервера"
	}

	/// Ссылка на слой презентации
	weak var presenter: DetailsPresentationProtocol?

	private var allSordedOrders: [Order] = []
	private var currentOrder: Order?
	private var indexOfCurrentOrder: Int?

	/// Инициализатор бизнес-слоя VIPER-молудя детальной информации о заказе
	/// - Parameter selectedOrderID: ID заказа по которому было осуществлено нажатие
	init(selectedOrderID: String) {

		reloadOrders { [weak self] (success, message, error, orders) in
			guard let self = self else { return }
			if success {
				self.allSordedOrders = orders
				self.findAndSetCurrentSelectedOrder(orderID: selectedOrderID)
			} else if let message = message {
				self.presenter?.present(message, .backendError)
			} else if let error = error {
				self.presenter?.present(error.localizedDescription, .unknownError)
			} else {
				self.presenter?.present(nil, .unknownError)
			}
		}
	}
}

private extension DetailsInteractor {
	func reloadOrders(completionHandler: ((_ sucess: Bool, _ message: String?, _ error: Error?, _ orders: [Order])->Void)?) {
		var allOrders = Mocker.shared.getAllOrders()
		sortOrders(&allOrders)
		completionHandler?(true, nil, nil, allOrders)
	}

	func sortOrders(_ orders: inout [Order]) {
		// Тут может быть осуществлена любая сортировка
	}

	func findAndSetCurrentSelectedOrder(orderID: String) {
		for (index, item) in allSordedOrders.enumerated() {
			if item.orderID == orderID {
				currentOrder = item
				indexOfCurrentOrder = index
				break
			}
		}
	}

	func startDataSending() {
		guard let indexOfCurrentOrder = indexOfCurrentOrder else { return }
		presenter?.pushCountOfOrders(allSordedOrders.count)
		//presenter?.pushIndexOfCurrentOrder(indexOfCurrentOrder)
		presenter?.pushAllOrders(allSordedOrders)
		presenter?.presentCurrentSelectedOrder(at: indexOfCurrentOrder)
	}
}

// MARK: - DetailsInteractionProtocol
extension DetailsInteractor: DetailsInteractionProtocol {
	func notifyPresenterReady() {
		startDataSending()
	}
}
