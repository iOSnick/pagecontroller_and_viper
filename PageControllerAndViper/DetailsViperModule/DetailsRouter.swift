//
//  DetailsRouter.swift
//  PageControllerAndViper
//
//  Created by 18907889 on 28.10.2021.
//

/// Протокол взаимодействия со слоем роутинга
protocol DetailsRoutingProtocol {
	func route(to target: DetailsRouter.Target)
}

/// Класс роутера VIPER-модуля детальной информации по заказу
final class DetailsRouter {

	enum Target {
		case repeatOrderView
	}
}

// MARK: - DetailsRoutingProtocol
extension DetailsRouter: DetailsRoutingProtocol {
	func route(to target: Target) {

	}
}
