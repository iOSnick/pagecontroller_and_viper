//
//  Mocker.swift
//  PageControllerAndViper
//
//  Created by Vikhlyaev Serge on 18.10.2021.
//

/// Проткол взаимодействия с мокером
protocol MockerProtocol {

	/// Метод, возвращающий количество viewModel-ей
	func countOfOrders() -> Int?

	/// Запрос на получение viewModel-и по индексу массива
	/// - Parameter index: индекс нужной viewModel-и в общем массиве viewModel-ей
	func getViewModel(for index: Int) -> MainScreenViewModelProtocol?

	/// Запрос на получение всех объектов 
	func getAllOrders() -> [Order]
}

/// Объект, имитирующий получение списков заказов
final class Mocker {

	static let shared: MockerProtocol = Mocker()

	private var orders: [Order] = []

	private init() {
		self.orders = createMockOrders()
	}
}

// MARK: - MockerProtocol
extension Mocker: MockerProtocol {

	func countOfOrders() -> Int? {
		orders.count
	}

	func getViewModel(for index: Int) -> MainScreenViewModelProtocol? {
		guard index < orders.count else { return nil }
		let order = orders[index]
		return order.getMainScreenViewModel()
	}

	func getAllOrders() -> [Order] {
		orders
	}
}

// MARK: - Private
private extension Mocker {
	func createMockOrders() -> [Order] {
		[
			Order(orderID: "1",
				  orderTitle: "ПРОДУКТЫ С ФЕРМЫ",
				  orderType: .products,
				  status: .successfulAndClosed,
				  operations: [.repeatOrder, .sendFeedback]),

			Order(orderID: "2",
				  orderTitle: "СЕМЁРОЧКА",
				  orderType: .products,
				  status: .successfulAndClosed,
				  operations: [.repeatOrder, .sendFeedback]),

			Order(orderID: "3",
				  orderTitle: "ПАРИКМАХЕРСКАЯ",
				  orderType: .service,
				  status: .successfulAndClosed,
				  operations: [.repeatOrder, .sendFeedback]),

			Order(orderID: "4",
				  orderTitle: "KFT",
				  orderType: .products,
				  status: .successfulAndClosed,
				  operations: [.repeatOrder, .sendFeedback]),

			Order(orderID: "5",
				  orderTitle: "ШИНОМОНТАЖ",
				  orderType: .service,
				  status: .successfulAndClosed,
				  operations: [.repeatOrder, .sendFeedback]),

			Order(orderID: "6",
				  orderTitle: "ДОСТАВКА ОТ ШЕРИФА",
				  orderType: .products,
				  status: .successfulAndClosed,
				  operations: [.repeatOrder, .sendFeedback]),

			Order(orderID: "7",
				  orderTitle: "СТОМАТОЛОГИЯ \"ПО МОДУЛУ 32\"",
				  orderType: .service,
				  status: .successfulAndClosed,
				  operations: [.repeatOrder, .sendFeedback]),

			Order(orderID: "8",
				  orderTitle: "СЕМЁРОЧКА",
				  orderType: .products,
				  status: .successfulAndClosed,
				  operations: [.repeatOrder, .sendFeedback])
		]
	}
}
