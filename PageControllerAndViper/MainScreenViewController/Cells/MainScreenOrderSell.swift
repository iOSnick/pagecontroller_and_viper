//
//  MainScreenOrderSell.swift
//  PageControllerAndViper
//
//  Created by 18907889 on 22.10.2021.
//

import UIKit

/// Класс ячейки для отображения заказа в таблице на главном экране
final class MainScreenOrderSell: UITableViewCell {

	@IBOutlet weak var orderImageView: UIImageView!
	@IBOutlet weak var orderTitleLabel: UILabel!
	@IBOutlet weak var orderSubtitleLabel: UILabel!

	private(set) var viewModel: MainScreenViewModelProtocol?

	func configure(with viewModel: MainScreenViewModelProtocol) {

		self.viewModel = viewModel

		orderTitleLabel?.text = viewModel.orderTitle
		orderSubtitleLabel?.text = "ORDER ID: \(viewModel.orderID)"
		orderImageView?.image = viewModel.orderType == .products ? UIImage(systemName: "bag.badge.plus.fill") : UIImage(systemName: "wrench.and.screwdriver")
	}

	override func prepareForReuse() {
		super.prepareForReuse()
		orderImageView.image = nil
		orderTitleLabel.text = ""
		orderSubtitleLabel.text = ""
	}
}
