//
//  MainScreenRouter.swift
//  PageControllerAndViper
//
//  Created by Vikhlyaev Serge on 22.10.2021.
//

import UIKit

/// Протокол взаимодействия с роутером главного экрана
protocol MainScreenRoutableProtocol {

	/// Метод, осуществляющий перехо на выбранный объект перехода
	/// - Parameter target: объект перехода
	func route(to target: MainScreenRouter.Target)
}

/// Роутер главного экрана
final class MainScreenRouter {

	/// Перечисление объектов перехода
	enum Target {

		/// Объект экрана детальной информации
		case detailsModule(String)
	}

	/// Слабая ссылка на UINavigationController
	private weak var navigationController: UINavigationController?

	/// Инициализатор роутера для главного экрана
	/// - Parameter navigationController: UINavigationController, на стек которого планируется наложение viewController-ов
	init(navigationController: UINavigationController?) {
		self.navigationController = navigationController
	}
}

// MARK: - MainScreenRoutingProtocol
extension MainScreenRouter: MainScreenRoutableProtocol {
	func route(to target: Target) {
		switch target {
		case .detailsModule(let orderID):
			let detailsAssembly = DetailsAssembly(orderID: orderID)
			let mainStoryboard: UIStoryboard = UIStoryboard(name: "Main", bundle: nil)
			var viewController: UIViewController = mainStoryboard.instantiateViewController(withIdentifier: "DetailsViewController") as! DetailsViewController
			detailsAssembly.configure(view: &viewController)
			navigationController?.pushViewController(viewController, animated: true)
		}
	}
}
