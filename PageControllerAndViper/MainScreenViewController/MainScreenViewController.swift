//
//  ViewControllerWithTable.swift
//  PageControllerAndViper
//
//  Created by Vikhlyaev Serge on 18.10.2021.
//

import UIKit

/// Протокол отображения событий на главном экране по инициативе presenter-а
protocol MainScreenDisplayingProtocol: AnyObject {}

/// ViewController с таблицей
final class MainScreenViewController: UIViewController {
	/// Таблица
	@IBOutlet weak var tableView: UITableView!

	/// Ссылка на объект презентера (инициализируется только при запросе)
	private var presenter: MainScreenEventIntersepter {
		let presenter = MainScreenPresenter(view: self)
		let router = MainScreenRouter(navigationController: self.navigationController)
		presenter.router = router
		return presenter
	}

	override func viewDidLoad() {
		super.viewDidLoad()

		// Непринужденно настраиваем таблицу
		tableView.delegate = self
		tableView.dataSource = self
	}
}

// MARK: - Private
private extension MainScreenViewController {

	/// Метод конфигурирования стандартной ячейки UITableViewCell для отображения в таблице
	/// - Parameters:
	///   - defaultCell: стандартная ячейка
	///   - order: viewModel заказа
	func configure(_ cell: inout MainScreenOrderSell,
				   for orderMainScreenViewModel: MainScreenViewModelProtocol) {
		cell.configure(with: orderMainScreenViewModel)
	}
}

// MARK: - MainScreenDisplayingProtocol
extension MainScreenViewController: MainScreenDisplayingProtocol {

}

// MARK: - UITableViewDelegate & UITableViewDataStore
extension MainScreenViewController: UITableViewDelegate & UITableViewDataSource {
	func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
		Mocker.shared.countOfOrders() ?? 0
	}

	func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
		guard
			var cell = tableView.dequeueReusableCell(withIdentifier: String(describing: MainScreenOrderSell.self)) as? MainScreenOrderSell,
			let viewModel = Mocker.shared.getViewModel(for: indexPath.row)
		else { return UITableViewCell () }

		configure(&cell, for: viewModel)
		return cell
	}

	func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
		tableView.deselectRow(at: indexPath, animated: true)
		guard
			let cell = tableView.cellForRow(at: indexPath) as? MainScreenOrderSell,
			let orderID = cell.viewModel?.order?.orderID
		else { return }
		presenter.didSelect(orderWith: orderID)
	}
}
