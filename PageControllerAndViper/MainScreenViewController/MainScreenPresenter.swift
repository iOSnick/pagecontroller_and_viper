//
//  MainScreenPresenter.swift
//  PageControllerAndViper
//
//  Created by Vikhlyaev Serge on 22.10.2021.
//

/// Протокол перехвата событий view
protocol MainScreenEventIntersepter {

	/// Был выбран заказ с id
	/// - Parameter id: id выбранного заказа
	func didSelect(orderWith id: String)
}

/// Класс presenter для главного экрана
final class MainScreenPresenter {

	weak var view: MainScreenDisplayingProtocol?
	var router: MainScreenRoutableProtocol?

	init(view: MainScreenDisplayingProtocol) {
		self.view = view
	}
}

// MARK: - MainScreenEventIntersepter
extension MainScreenPresenter: MainScreenEventIntersepter {
	func didSelect(orderWith id: String) {
		router?.route(to: .detailsModule(id))
	}
}
