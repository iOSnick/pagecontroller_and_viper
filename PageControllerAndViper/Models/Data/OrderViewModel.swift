//
//  OrderViewModel.swift
//  PageControllerAndViper
//
//  Created by Vikhlyaev Serge on 18.10.2021.
//

import Foundation

/// Протокол для описания viewModel-и, демонстрируемой на гравном экране (в таблице)
protocol MainScreenViewModelProtocol {

	/// Ссылка на объект заказа
	var order: Order? { get }
	/// ID заказа
	var orderID: String { get }
	/// Название заказа
	var orderTitle: String { get }
	/// Тип заказа
	var orderType: Order.OrderType { get }
}

struct MainScreenViewModel: MainScreenViewModelProtocol {
	var order: Order?
	let orderID: String
	let orderTitle: String
	let orderType: Order.OrderType
}
