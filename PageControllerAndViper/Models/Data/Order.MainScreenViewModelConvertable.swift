//
//  Order.MainScreenViewModelConvertable.swift
//  PageControllerAndViper
//
//  Created by 18907889 on 31.10.2021.
//

// MARK: - MainScreenViewModelConvertable
extension Order: MainScreenViewModelConvertable {
	func getMainScreenViewModel() -> MainScreenViewModelProtocol {
		MainScreenViewModel(order: self,
							orderID: orderID,
							orderTitle: orderTitle,
							orderType: orderType)
	}
}
