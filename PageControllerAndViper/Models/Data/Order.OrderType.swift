//
//  Order.Type.swift
//  PageControllerAndViper
//
//  Created by Vikhlyaev Serge on 18.10.2021.
//

// MARK: - OrderType
extension Order {

	/// Тип заказа
	enum OrderType {
		/// Продуктовый заказ
		case products
		/// Заказ услуги
		case service
	}

	/// Состояние заказа
	enum Status {
		/// Заказ в процессе исполнения
		case inProcess
		/// Заказ закрыт с успехом
		case successfulAndClosed
		/// Заказ закрыт неуспешно
		case failureAndClosed
		/// Заказ отменен
		case cancelled
	}

	/// Типы операций по заказу
	enum AvailableOperation {
		/// Отменить заказ
		case cancel
		/// Повторить заказ
		case repeatOrder
		/// Отправить отзыв
		case sendFeedback
		/// Позвонить исполнителю/курьеру
		case call
	}
}
