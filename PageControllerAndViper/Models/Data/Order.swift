//
//  Order.swift
//  PageControllerAndViper
//
//  Created by Vikhlyaev Serge on 18.10.2021.
//

import Foundation

/// Объект заказа
struct Order {

	/// id Заказа
	let orderID: String

	/// Название заказа
	let orderTitle: String

	/// Тип заказа
	let orderType: Order.OrderType

	/// Статус (состояние) заказа
	let status: Order.Status

	/// Доступные операции по заказу
	let operations: [Order.AvailableOperation]
}
