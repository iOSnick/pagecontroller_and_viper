//
//  UsingColors.swift
//  PageControllerAndViper
//
//  Created by 18907889 on 01.11.2021.
//

import UIKit

enum UsingColors {
	static let blue = UIColor(red: 5.0/255.0, green: 13.0/255.0, blue: 136.0/255.0, alpha: 1.0).cgColor
	static let green = UIColor(red: 13.0/255.0, green: 37.0/255.0, blue: 14.0/255.0, alpha: 1.0).cgColor
	static let white = UIColor.white.cgColor
}

