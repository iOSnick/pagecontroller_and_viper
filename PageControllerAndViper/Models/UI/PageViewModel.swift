//
//  PageViewModel.swift
//  PageControllerAndViper
//
//  Created by 18907889 on 29.10.2021.
//

/// Протокол viewModel-и для 1й страницы детальной информации о заказе
protocol PageViewModelProtocol {

	/// Текстовка для отображения в поле ID
	var idLabelText: String { get }

	/// Текстовка для отображения в поле OrderName
	var orderNameLabelText: String { get }

	/// URL-адрес изображения, которое необходимо отобразить в заказе
	var imageURLString: String? { get }

	/// Доступные по заказу операции
	var operations: [Order.AvailableOperation] { get }

	/// Объект, перехватывающий нажатие на ячейку операции
	var selectOperationEventIntercepter: SelectOperationEventInterceptable? { get }

	/// Цвет заголовка
	var headerColor: DetailsViewController.HeaderColor { get }
}

/// Класс viewModel-и для одной страницы детальной информации о заказе
final class PageViewModel: PageViewModelProtocol {

	let idLabelText: String
	let orderNameLabelText: String
	let imageURLString: String?
	let operations: [Order.AvailableOperation]
	let selectOperationEventIntercepter: SelectOperationEventInterceptable?
	var headerColor: DetailsViewController.HeaderColor

	/// Инициализатор объекта viewModel-и для одной страницы детальной информации о заказе
	/// - Parameters:
	///   - id: Текстовка для отображения в поле ID
	///   - orderName: Текстовка для отображения в поле OrderName
	///   - imageURL: URL-адрес изображения, которое необходимо отобразить в заказе
	///   - page: Сопутствующая страница вспомогательных функций и информации об этом заказе
	init(id: String,
		 orderName: String,
		 imageURL: String?,
		 orderType: Order.OrderType,
		 operations: [Order.AvailableOperation],
		 interceptor: SelectOperationEventInterceptable?) {
		idLabelText = id
		orderNameLabelText = orderName
		imageURLString = imageURL
		self.operations = operations
		self.selectOperationEventIntercepter = interceptor
		headerColor = orderType == .products ? .green : .blue
	}
}
