//
//  MainScreenViewModelConvertable.swift
//  PageControllerAndViper
//
//  Created by 18907889 on 31.10.2021.
//

/// Протокол получения viewModel-и для главного экрана
protocol MainScreenViewModelConvertable {

	/// Метод получения viewModel-и главного экрана
	func getMainScreenViewModel() -> MainScreenViewModelProtocol
}
